<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VehicleAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Jana Draf Perjanjian';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";
$js = "
$('.ajax').click(function(e){
    $.ajax({
       url: 'index.php?r=vehicle-assignment/approve',
       data: {id: 32},
       success: function(data) {
           // process data

       }
    });
    return false;
});
";
// $this->registerJs($search);
// $this->registerJs($js);
?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#draft-generator-form" data-toggle="tab">Draf Dokumen</a></li>
        <li><a href="#contract-form" data-toggle="tab">Kontrak</a></li>
        <li><a href="#contractor-form" data-toggle="tab">Syarikat</a></li>
    </ul>
    <div class="tab-content">
        <div class="active tab-pane" id="draft-generator-form">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
        <div class="tab-pane" id="contract-form">
            <?= $this->render('_formContract', [
                'model' => $contract,
            ]) ?>
        </div>
        <div class="tab-pane" id="contractor-form">
            <?= $this->render('_formContractor', [
                'model' => $company,
            ]) ?>
        </div>
    </div>
</div>
