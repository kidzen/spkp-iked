<?php

namespace common\components\helper;

use Yii;

/**
 * This is the model class for table "cases".
 */
class Constant
{
    //status field
    const STATUS_DEPARTMENT_PENDING = 11;
    const STATUS_DEPARTMENT_APPROVED = 12;
    const STATUS_DEPARTMENT_REJECTED = 13;
    const STATUS_DEPARTMENT_CASE_TO_COURT = 14;
    const STATUS_DEPARTMENT_DISPOSED = 15;

    const STATUS_COURT_PENDING = 21;
    const STATUS_COURT_APPROVED = 22;
    const STATUS_COURT_REJECTED = 23;
    const STATUS_COURT_DISPOSED = 24;

    const STATUS_COURT_PENDING_TRIAL = 25;
    const STATUS_COURT_ON_TRIAL = 26;
    const STATUS_COURT_RESULT = 27;

    //deleted field
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;



    //record_type field
    const CASE_HANDOVER = 11;

        const CASE_SEBUTAN = 1;                                        //  a
        const CASE_BICARA = 2;                                         //  b
        const CASE_TARIK_BALIK = 3;                                    //  c
        const CASE_MENGAKU_SALAH = 4;   //consider case is closed      //  d
        const CASE_BATAL = 5;           //consider case is closed      //  e
        const CASE_WARAN = 6;                                          //  f
        const CASE_WARAN_EXECUTED = 7;                                 //  g
        const CASE_TANGGUH = 8;                                        //  h

    const CASE_NOT_HANDOVER = 12;       //case with no court file       //  i

    const CASE_SOLVED_IN_COURT = 21;   //closed                        //  j
    const CASE_SOLVED_IN_MPSP = 22;                                     //  k


// case is closed/solved in court if (d) or (e)
// case is forward if (abc) or (fghi) or
}



