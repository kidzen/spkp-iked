<?php

namespace common\components;

use yii\base\DynamicModel as BaseDynamicModel;


class DynamicModel extends BaseDynamicModel
{
    public $attributeLabels = [];

    public function attributeLabels(){
        return $this->attributeLabels;
    }
}
