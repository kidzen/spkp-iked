<?php

namespace common\models;

use Yii;
use \common\models\base\RankRequirement as BaseRankRequirement;

/**
 * This is the model class for table "rank_requirement".
 */
class RankRequirement extends BaseRankRequirement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['achievement_id', 'rank_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['remark'], 'string', 'max' => 255]
        ]);
    }
	
}
