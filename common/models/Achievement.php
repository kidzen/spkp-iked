<?php

namespace common\models;

use Yii;
use \common\models\base\Achievement as BaseAchievement;

/**
 * This is the model class for table "achievement".
 */
class Achievement extends BaseAchievement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'remark'], 'string', 'max' => 255]
        ]);
    }
	
}
