<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\Achievement]].
 *
 * @see \common\models\query\Achievement
 */
class AchievementQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\query\Achievement[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\Achievement|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
