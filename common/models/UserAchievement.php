<?php

namespace common\models;

use Yii;
use \common\models\base\UserAchievement as BaseUserAchievement;

/**
 * This is the model class for table "user_achievement".
 */
class UserAchievement extends BaseUserAchievement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['achievement_id', 'user_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['remark'], 'string', 'max' => 255]
        ]);
    }
	
}
