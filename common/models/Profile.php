<?php

namespace common\models;

use Yii;
use \common\models\base\Profile as BaseProfile;

/**
 * This is the model class for table "profile".
 */
class Profile extends BaseProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_no', 'name'], 'required'],
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['staff_no'], 'string', 'max' => 20],
            [['name', 'position'], 'string', 'max' => 100],
            [['department'], 'string', 'max' => 255],
            [['staff_no'], 'unique']
        ]);
    }
	
}
