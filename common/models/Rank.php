<?php

namespace common\models;

use Yii;
use \common\models\base\Rank as BaseRank;

/**
 * This is the model class for table "rank".
 */
class Rank extends BaseRank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['next_rank', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'remark'], 'string', 'max' => 255]
        ]);
    }
	
}
