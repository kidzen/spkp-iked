<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Approver */

?>
<div class="approver-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'draftDocument.name',
            'label' => 'Draft Document',
        ],
        [
            'attribute' => 'approver.username',
            'label' => 'Approver',
        ],
        [
            'attribute' => 'approveLevel.approve_level',
            'label' => 'Approve Level',
        ],
        'approved',
        'approved_at',
        'approved_by',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>