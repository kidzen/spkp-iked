<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Approver */

$this->title = 'Create Approver';
$this->params['breadcrumbs'][] = ['label' => 'Approver', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="approver-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
