<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draft Document', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-document-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Document'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        [
            'attribute' => 'contractTemplate.id',
            'label' => 'Contract Template',
        ],
        [
            'attribute' => 'draftStatus.name',
            'label' => 'Draft Status',
        ],
        'description',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerApprover->totalCount){
    $gridColumnApprover = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'approver.username',
                'label' => 'Approver'
            ],
            [
                'attribute' => 'approveLevel.approve_level',
                'label' => 'Approve Level'
            ],
            'approved',
            'approved_at',
            'approved_by',
            'remark',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerApprover,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-approver']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Approver'),
        ],
        'export' => false,
        'columns' => $gridColumnApprover
    ]);
}
?>
    </div>
</div>
