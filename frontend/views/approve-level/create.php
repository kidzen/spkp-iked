<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ApproveLevel */

$this->title = 'Create Approve Level';
$this->params['breadcrumbs'][] = ['label' => 'Approve Level', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="approve-level-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
