<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->draftDocuments,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        [
                'attribute' => 'contract.name',
                'label' => 'Contract'
            ],
        [
                'attribute' => 'draftStatus.name',
                'label' => 'Draft Status'
            ],
        'description',
        [
                'attribute' => 'signator10.name',
                'label' => 'Signator1'
            ],
        [
                'attribute' => 'signator20.name',
                'label' => 'Signator2'
            ],
        'remark',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'draft-document'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
