<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContractTemplateAssignment */

$this->title = 'Create Contract Template Assignment';
$this->params['breadcrumbs'][] = ['label' => 'Contract Template Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-template-assignment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
