<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ContractTemplateAssignment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Contract Template Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-template-assignment-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contract Template Assignment'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'template.name',
            'label' => 'Template',
        ],
        [
            'attribute' => 'contract.name',
            'label' => 'Contract',
        ],
        'description',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerDraftDocument->totalCount){
    $gridColumnDraftDocument = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
            'file',
                        [
                'attribute' => 'draftStatus.name',
                'label' => 'Draft Status'
            ],
            'description',
            'remark',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftDocument,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-document']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Draft Document'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftDocument
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerDraftParam->totalCount){
    $gridColumnDraftParam = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'paramList.name',
                'label' => 'Param List'
            ],
            'param',
            'remark',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftParam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-param']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Draft Param'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftParam
    ]);
}
?>
    </div>
</div>
