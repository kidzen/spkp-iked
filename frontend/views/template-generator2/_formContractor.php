<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */
// var_dump(Yii::$app->getUrlManager()->createUrl('contractor/ajaxDaftar'));die();
/*
                 url: "<?php echo Yii::$app->getUrlManager()->createUrl('contractor/create'); ?>",
*/
?>

<div class="contractor-form">
    <?php Pjax::begin() ?>

    <?php $form = ActiveForm::begin(['id'=>'contractor-form']); ?>
    <?= $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'reg_no')->textInput(['maxlength' => true, 'placeholder' => 'Reg No']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'address1')->textInput(['maxlength' => true, 'placeholder' => 'Address1']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'address2')->textInput(['maxlength' => true, 'placeholder' => 'Address2']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'poscode')->textInput(['placeholder' => 'Poscode']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'district1')->textInput(['maxlength' => true, 'placeholder' => 'District1']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'district2')->textInput(['maxlength' => true, 'placeholder' => 'District2']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'state')->textInput(['maxlength' => true, 'placeholder' => 'State']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'contact_no')->textInput(['maxlength' => true, 'placeholder' => 'Contact No']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'fax_no')->textInput(['maxlength' => true, 'placeholder' => 'Fax No']) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Daftar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>

</div>
