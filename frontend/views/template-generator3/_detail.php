<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */

?>
<div class="draft-document-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->name) ?></h2>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        [
            'attribute' => 'template.name',
            'label' => 'Template',
        ],
        [
            'attribute' => 'contract.name',
            'label' => 'Kontrak',
        ],
        [
            'attribute' => 'draftStatus.name',
            'label' => 'Draft Status',
        ],
        'description',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
