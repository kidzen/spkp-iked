<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Approver',
        'relID' => 'approver',
        'value' => \yii\helpers\Json::encode($model->approvers),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
$wizard_config = [
    'id' => 'stepwizard',
    'steps' => [
        1 => [
            'title' => 'Step 1',
            'icon' => 'glyphicon glyphicon-cloud-download',
            'content' => '<h3>Step 1</h3>This is step 1',
            'buttons' => [
                'next' => [
                    'title' => 'Forward',
                    'options' => [
                        'class' => 'disabled'
                    ],
                 ],
             ],
        ],
        2 => [
            'title' => 'Step 2',
            'icon' => 'glyphicon glyphicon-cloud-upload',
            'content' => '<h3>Step 2</h3>This is step 2',
            'skippable' => true,
        ],
        3 => [
            'title' => 'Step 3',
            'icon' => 'glyphicon glyphicon-transfer',
            'content' => '<h3>Step 3</h3>This is step 3',
        ],
    ],
    'complete_content' => "You are done!", // Optional final screen
    'start_step' => 2, // Optional, start with a specific step
];
?>

<?= \drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>

<div class="draft-document-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default">
        <div class="panel-heading">Jana Draf Dokumen</div>
        <div class="panel-body">
            <?= $form->errorSummary($model); ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'template_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\DraftTemplate::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                        'options' => ['placeholder' => 'Choose Draft template'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Kontrak</div>
                        <div class="panel-body">
                            <?php echo Yii::$app->controller->renderPartial('_formContract',['model' => $contract]) ?>
                            <?php //echo $form->field($model->contract, 'contract_no')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Kontrak Sedia Ada</div>
                        <div class="panel-body">
                            <?= $form->field($model, 'contract_id')->widget(\kartik\widgets\Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map(\common\models\Contract::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                                'options' => ['placeholder' => 'Choose Contract'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'status')->textInput(['placeholder' => 'Status']) ?>
                </div>
                <div class="col-md-12">
                    <?php
                    $forms = [
                        [
                            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Approver'),
                            'content' => $this->render('_formApprover', [
                                'row' => \yii\helpers\ArrayHelper::toArray($model->approvers),
                            ]),
                        ],
                    ];
                    echo kartik\tabs\TabsX::widget([
                        'items' => $forms,
                        'position' => kartik\tabs\TabsX::POS_ABOVE,
                        'encodeLabels' => false,
                        'pluginOptions' => [
                            'bordered' => true,
                            'sideways' => true,
                            'enableCache' => false,
                        ],
                    ]);
                    ?>
                </div>
            </div>




            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Daftar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
            </div>


        </div>
    </div>
            <?php ActiveForm::end(); ?>
</div>
<?php
$script = <<<JS
$(function () {
    $('.hint-block').each(function () {
        var hint = $(this);
        hint.parent().find('label').addClass('help').popover({
            html: true,
            trigger: 'hover',
            placement: 'right',
            content: hint.html()
        });
        hint.remove();
    });
});

JS;
$this->registerJs($script);
?>
