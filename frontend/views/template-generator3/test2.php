<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Contract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contract-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>
<div class="row">

<div class="col-md-3" style="display:none">
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'contract_no')->textInput(['maxlength' => true, 'placeholder' => 'Contract No']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'product')->textInput(['maxlength' => true, 'placeholder' => 'Product']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'company_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Contractor::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Contractor'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'bon')->textInput(['placeholder' => 'Bon']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'bon_text')->textInput(['maxlength' => true, 'placeholder' => 'Bon Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'compensation')->textInput(['placeholder' => 'Compensation']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'compensation_text')->textInput(['maxlength' => true, 'placeholder' => 'Compensation Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'insurance')->textInput(['placeholder' => 'Insurance']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'insurance_text')->textInput(['maxlength' => true, 'placeholder' => 'Insurance Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'mp_insurance')->textInput(['placeholder' => 'Mp Insurance']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'mp_insurance_text')->textInput(['maxlength' => true, 'placeholder' => 'Mp Insurance Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'cost')->textInput(['placeholder' => 'Cost']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'cost_text')->textInput(['maxlength' => true, 'placeholder' => 'Cost Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'irt_duration')->textInput(['placeholder' => 'Irt Duration']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'irt_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Irt Duration Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'irt_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Irt Duration Type']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'rt_duration')->textInput(['placeholder' => 'Rt Duration']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'rt_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Rt Duration Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'rt_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Rt Duration Type']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'contract_duration')->textInput(['placeholder' => 'Contract Duration']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'contract_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Contract Duration Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'contract_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Contract Duration Type']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'delivery_duration')->textInput(['placeholder' => 'Delivery Duration']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'delivery_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Delivery Duration Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'delivery_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Delivery Duration Type']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'warranty_duration')->textInput(['placeholder' => 'Warranty Duration']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'warranty_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Warranty Duration Text']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'warranty_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Warranty Duration Type']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'agreement_start_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Agreement Start Date',
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'agreement_end_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Agreement End Date',
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'expected_start_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Expected Start Date',
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'expected_end_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Expected End Date',
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'start_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Start Date',
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'end_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose End Date',
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'status')->textInput(['placeholder' => 'Status']) ?>
</div>
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Daftar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
