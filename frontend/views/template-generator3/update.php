<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */

$this->title = 'Update Draf Perjanjian: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draf Perjanjian', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="draft-document-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
