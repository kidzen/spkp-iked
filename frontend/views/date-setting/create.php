<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DateSetting */

$this->title = 'Create Date Setting';
$this->params['breadcrumbs'][] = ['label' => 'Date Setting', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="date-setting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
