<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Contractor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contractor-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contractor'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'reg_no',
        'address1',
        'address2',
        'poscode',
        'district1',
        'district2',
        'state',
        'contact_no',
        'fax_no',
        'field_code',
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerContract->totalCount){
    $gridColumnContract = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'contract_no',
            'name',
            'product',
            'description',
                        'bon',
            'bon_text',
            'compensation',
            'compensation_text',
            'insurance',
            'insurance_text',
            'mp_insurance',
            'mp_insurance_text',
            'cost',
            'cost_text',
            'irt_duration',
            'irt_duration_text',
            'irt_duration_type',
            'rt_duration',
            'rt_duration_text',
            'rt_duration_type',
            'contract_duration',
            'contract_duration_text',
            'contract_duration_type',
            'delivery_duration',
            'delivery_duration_text',
            'delivery_duration_type',
            'warranty_duration',
            'warranty_duration_text',
            'warranty_duration_type',
            'deficiency_duration',
            'deficiency_duration_text',
            'deficiency_duration_type',
            'agreement_start_date',
            'agreement_end_date',
            'expected_start_date',
            'expected_end_date',
            'start_date',
            'end_date',
            'remark',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContract,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contract']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Contract'),
        ],
        'export' => false,
        'columns' => $gridColumnContract
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerContractorStaff->totalCount){
    $gridColumnContractorStaff = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
            'ic',
            'position',
                        'remark',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContractorStaff,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contractor-staff']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Contractor Staff'),
        ],
        'export' => false,
        'columns' => $gridColumnContractorStaff
    ]);
}
?>
    </div>
</div>
