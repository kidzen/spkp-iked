<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Contract */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'DocumentChecklist',
        'relID' => 'document-checklist',
        'value' => \yii\helpers\Json::encode($model->documentChecklists),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'DraftDocument',
        'relID' => 'draft-document',
        'value' => \yii\helpers\Json::encode($model->draftDocuments),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'JtContractMeeting',
        'relID' => 'jt-contract-meeting',
        'value' => \yii\helpers\Json::encode($model->jtContractMeetings),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Note',
        'relID' => 'note',
        'value' => \yii\helpers\Json::encode($model->notes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="contract-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'contract_no')->textInput(['maxlength' => true, 'placeholder' => 'Contract No']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?= $form->field($model, 'product')->textInput(['maxlength' => true, 'placeholder' => 'Product']) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

    <?= $form->field($model, 'company_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Contractor::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Contractor'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'bon')->textInput(['placeholder' => 'Bon']) ?>

    <?= $form->field($model, 'bon_text')->textInput(['maxlength' => true, 'placeholder' => 'Bon Text']) ?>

    <?= $form->field($model, 'compensation')->textInput(['placeholder' => 'Compensation']) ?>

    <?= $form->field($model, 'compensation_text')->textInput(['maxlength' => true, 'placeholder' => 'Compensation Text']) ?>

    <?= $form->field($model, 'insurance')->textInput(['placeholder' => 'Insurance']) ?>

    <?= $form->field($model, 'insurance_text')->textInput(['maxlength' => true, 'placeholder' => 'Insurance Text']) ?>

    <?= $form->field($model, 'mp_insurance')->textInput(['placeholder' => 'Mp Insurance']) ?>

    <?= $form->field($model, 'mp_insurance_text')->textInput(['maxlength' => true, 'placeholder' => 'Mp Insurance Text']) ?>

    <?= $form->field($model, 'cost')->textInput(['placeholder' => 'Cost']) ?>

    <?= $form->field($model, 'cost_text')->textInput(['maxlength' => true, 'placeholder' => 'Cost Text']) ?>

    <?= $form->field($model, 'irt_duration')->textInput(['placeholder' => 'Irt Duration']) ?>

    <?= $form->field($model, 'irt_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Irt Duration Text']) ?>

    <?= $form->field($model, 'irt_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Irt Duration Type']) ?>

    <?= $form->field($model, 'rt_duration')->textInput(['placeholder' => 'Rt Duration']) ?>

    <?= $form->field($model, 'rt_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Rt Duration Text']) ?>

    <?= $form->field($model, 'rt_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Rt Duration Type']) ?>

    <?= $form->field($model, 'contract_duration')->textInput(['placeholder' => 'Contract Duration']) ?>

    <?= $form->field($model, 'contract_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Contract Duration Text']) ?>

    <?= $form->field($model, 'contract_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Contract Duration Type']) ?>

    <?= $form->field($model, 'delivery_duration')->textInput(['placeholder' => 'Delivery Duration']) ?>

    <?= $form->field($model, 'delivery_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Delivery Duration Text']) ?>

    <?= $form->field($model, 'delivery_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Delivery Duration Type']) ?>

    <?= $form->field($model, 'warranty_duration')->textInput(['placeholder' => 'Warranty Duration']) ?>

    <?= $form->field($model, 'warranty_duration_text')->textInput(['maxlength' => true, 'placeholder' => 'Warranty Duration Text']) ?>

    <?= $form->field($model, 'warranty_duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Warranty Duration Type']) ?>

    <?= $form->field($model, 'agreement_start_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Agreement Start Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'agreement_end_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Agreement End Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'expected_start_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Expected Start Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'expected_end_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Expected End Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'start_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Start Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'end_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose End Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <?= $form->field($model, 'status')->textInput(['placeholder' => 'Status']) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('DocumentChecklist'),
            'content' => $this->render('_formDocumentChecklist', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->documentChecklists),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('DraftDocument'),
            'content' => $this->render('_formDraftDocument', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->draftDocuments),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('JtContractMeeting'),
            'content' => $this->render('_formJtContractMeeting', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->jtContractMeetings),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Note'),
            'content' => $this->render('_formNote', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->notes),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Daftar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
