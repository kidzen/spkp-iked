<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Template generator', 'url' => ['/template-generator/generator']],
        ['label' => 'Database', 'items' => [
            ['label' => 'User', 'url' => ['/user/index']],
            ['label' => 'Role', 'url' => ['/role/index']],
            ['label' => 'Kontrak', 'url' => ['/contract/index']],
            ['label' => 'Kontraktor', 'url' => ['/contractor/index']],
            ['label' => 'Mesyuarat', 'url' => ['/meeting/index']],
            ['label' => 'Contract Meeting', 'url' => ['/jt-contract-meeting/index']],
            ['label' => 'Draft Template', 'url' => ['/draft-template/index']],
            ['label' => 'Draft Status', 'url' => ['/draft-status/index']],
            ['label' => 'Date Setting', 'url' => ['/date-setting/index']],
            ['label' => 'Draf Perjanjian', 'url' => ['/draft-document/index']],
            ['label' => 'Approver', 'url' => ['/approver/index']],
            ['label' => 'Approve Level', 'url' => ['/approve-level/index']],
            ['label' => 'Note', 'url' => ['/note/index']],
            ['label' => 'Document Checklist', 'url' => ['/document-checklist/index']],
        ]],
        // ['label' => 'About', 'url' => ['/site/about']],
        // ['label' => 'Contact', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
