<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Rank */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Rank', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rank-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Rank'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
            'attribute' => 'nextRank.name',
            'label' => 'Next Rank',
        ],
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Rank<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnRank = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
    ?>
</div>
    
    <div class="row">
<?php
if($providerRank->totalCount){
    $gridColumnRank = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
                        'description',
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerRank,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-rank']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Rank'),
        ],
        'export' => false,
        'columns' => $gridColumnRank
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerRankRequirement->totalCount){
    $gridColumnRankRequirement = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'achievement.name',
                'label' => 'Achievement'
            ],
                        'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerRankRequirement,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-rank-requirement']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Rank Requirement'),
        ],
        'export' => false,
        'columns' => $gridColumnRankRequirement
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerUser->totalCount){
    $gridColumnUser = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'username',
            'staff_no',
            'name',
                        'unit',
            [
                'attribute' => 'role.name',
                'label' => 'Role'
            ],
            'email:email',
            'password_hash',
            'auth_key',
            'password_reset_token',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerUser,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-user']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('User'),
        ],
        'export' => false,
        'columns' => $gridColumnUser
    ]);
}
?>

    </div>
</div>
