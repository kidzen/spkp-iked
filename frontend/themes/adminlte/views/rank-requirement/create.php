<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RankRequirement */

$this->title = 'Create Rank Requirement';
$this->params['breadcrumbs'][] = ['label' => 'Rank Requirement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rank-requirement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
