<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\RankRequirement */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rank Requirement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rank-requirement-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Rank Requirement'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'achievement.name',
            'label' => 'Achievement',
        ],
        [
            'attribute' => 'rank.name',
            'label' => 'Rank',
        ],
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Rank<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnRank = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'next_rank',
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
    ?>
</div>
    <div class="row">
        <h4>Achievement<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAchievement = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
    ?>
</div>
</div>
