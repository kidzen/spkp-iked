<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Achievement */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Achievement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Achievement'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerRankRequirement->totalCount){
    $gridColumnRankRequirement = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'rank.name',
                'label' => 'Rank'
            ],
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerRankRequirement,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-rank-requirement']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Rank Requirement'),
        ],
        'export' => false,
        'columns' => $gridColumnRankRequirement
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerUserAchievement->totalCount){
    $gridColumnUserAchievement = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'user.username',
                'label' => 'User'
            ],
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerUserAchievement,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-user-achievement']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('User Achievement'),
        ],
        'export' => false,
        'columns' => $gridColumnUserAchievement
    ]);
}
?>

    </div>
</div>
