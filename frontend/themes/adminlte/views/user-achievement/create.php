<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserAchievement */

$this->title = 'Create User Achievement';
$this->params['breadcrumbs'][] = ['label' => 'User Achievement', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-achievement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
