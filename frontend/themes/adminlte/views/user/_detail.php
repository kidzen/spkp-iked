<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

?>
<div class="user-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->username) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'staff_no',
        'name',
        [
            'attribute' => 'rank.name',
            'label' => 'Rank',
        ],
        'unit',
        [
            'attribute' => 'role.name',
            'label' => 'Role',
        ],
        'email:email',
        'password_hash',
        'auth_key',
        'password_reset_token',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>