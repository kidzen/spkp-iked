<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'User'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'staff_no',
        'name',
        [
            'attribute' => 'rank.name',
            'label' => 'Rank',
        ],
        'unit',
        [
            'attribute' => 'role.name',
            'label' => 'Role',
        ],
        'email:email',
        'password_hash',
        'auth_key',
        'password_reset_token',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Role<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnRole = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
    ?>
</div>
    <div class="row">
        <h4>Rank<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnRank = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'next_rank',
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
    ?>
</div>
    
    <div class="row">
<?php
if($providerUserAchievement->totalCount){
    $gridColumnUserAchievement = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'achievement.name',
                'label' => 'Achievement'
            ],
                        'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerUserAchievement,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-user-achievement']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('User Achievement'),
        ],
        'export' => false,
        'columns' => $gridColumnUserAchievement
    ]);
}
?>

    </div>
</div>
