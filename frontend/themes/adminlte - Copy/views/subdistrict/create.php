<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Subdistrict */

$this->title = 'Create Subdistrict';
$this->params['breadcrumbs'][] = ['label' => 'Subdistrict', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subdistrict-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
