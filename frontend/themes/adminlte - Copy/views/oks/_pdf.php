<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Oks */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Oks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oks-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Oks'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'ic',
        'address',
        'phone_no',
        'email:email',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerOksCase->totalCount){
    $gridColumnOksCase = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'case.file_no',
                'label' => 'Case'
            ],
                'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerOksCase,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Oks Case'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnOksCase
    ]);
}
?>
    </div>
</div>
