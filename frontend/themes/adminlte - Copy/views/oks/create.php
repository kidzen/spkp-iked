<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Oks */

$this->title = 'Create Oks';
$this->params['breadcrumbs'][] = ['label' => 'Oks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
