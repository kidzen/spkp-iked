<?php
use backend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

dmstr\web\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login-page img-layout">

<?php $this->beginBody() ?>

    <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->registerCss('
.img-layout{
	// background-color:green;
	// background-image:url("http://localhost/dakwaan/frontend/web/images/MPSP-building.jpg");
	background-image:url("http://localhost/dakwaan/frontend/web/images/MPSP5.jpg");
	background-size:100%;
}

') ?>
<?php $this->endPage() ?>
