<?php
use yii\helpers\Url;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div style="text-align: center">
                <img src="<?= Yii::getAlias('@web/').Url::to('/images/MPSP.png') ?>" style="width: 200px" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/>
                <!--<img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" style="width: 200px" alt="TUDM"/>-->
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Permohonan', 'icon' => 'database',
                        'items' => [
                            ['label' => 'Kes Jabatan', 'icon' => 'user', 'url' => ['/department-request/create']],
                            ['label' => 'Kes Mahkamah', 'icon' => 'user', 'url' => ['/court-request/create']],
                        ]
                    ],
                    [
                        'label' => 'Senarai', 'icon' => 'database',
                        'items' => [
                            ['label' => 'Kes Jabatan', 'icon' => 'user', 'url' => ['/department-request/index']],
                            ['label' => 'Kes Mahkamah', 'icon' => 'user', 'url' => ['/court-request/index']],
                        ]
                    ],
                    [
                        'label' => 'Analysis', 'icon' => 'database',
                        'items' => [
                            ['label' => 'Laporan Tahunan', 'icon' => 'user', 'url' => ['/report/yearly']],
                            ['label' => 'Laporan Bulanan', 'icon' => 'user', 'url' => ['/report/monthly']],
                            ['label' => 'Laporan Mingguan', 'icon' => 'user', 'url' => ['/report/weekly']],
                        ]
                    ],
                    [
                        'label' => 'Admin', 'icon' => 'database',
                        'items' => [
                            ['label' => 'Pengguna', 'icon' => 'user', 'url' => ['/user/index']],
                            ['label' => 'Profil', 'icon' => 'user', 'url' => ['/profile/index']],
                            ['label' => 'Akses', 'icon' => 'user', 'url' => ['/role/index']],
                            ['label' => 'Profil Oks', 'icon' => 'user', 'url' => ['/oks/index']],
                            ['label' => 'Kes Jabatan', 'icon' => 'user', 'url' => ['/department-case/index']],
                            ['label' => 'Kes Mahkamah', 'icon' => 'user', 'url' => ['/court-case/index']],
                            ['label' => 'Jabatan', 'icon' => 'user', 'url' => ['/department/index']],
                            ['label' => 'Geolokasi', 'icon' => 'user', 'url' => ['/geolocation/index']],
                            ['label' => 'Kesalahan', 'icon' => 'user', 'url' => ['/offence/index']],
                            ['label' => 'Lot Kesalahan', 'icon' => 'user', 'url' => ['/offence/index']],
                        ]
                    ],
                    [
                        // 'visible'=>false,
                        'label' => 'Dev tools',
                        'icon' => 'share',
                        // 'url' => '#',
                        'items' => [
                            [
                                'label' => 'Database',
                                'icon' => 'database',
                                'items' => [
                                        ['label' => 'User', 'icon' => 'user', 'url' => ['/user/index']],
                                        ['label' => 'Profile', 'icon' => 'user', 'url' => ['/profile/index']],
                                        ['label' => 'Role', 'icon' => 'user', 'url' => ['/role/index']],
                                        ['label' => 'Department Case', 'icon' => 'user', 'url' => ['/department-case/index']],
                                        ['label' => 'Court Cases', 'icon' => 'user', 'url' => ['/court-case/index']],
                                        ['label' => 'Case Record', 'icon' => 'user', 'url' => ['/case-record/index']],
                                        ['label' => 'Case Status', 'icon' => 'user', 'url' => ['/case-status/index']],
                                        ['label' => 'Case Lot', 'icon' => 'user', 'url' => ['/case-lot/index']],
                                        ['label' => 'Note', 'icon' => 'user', 'url' => ['/note/index']],
                                        ['label' => 'Document Attachment', 'icon' => 'user', 'url' => ['/document-attachement/index']],
                                        ['label' => 'Document Checklist', 'icon' => 'user', 'url' => ['/document-checklist/index']],
                                        ['label' => 'Document', 'icon' => 'user', 'url' => ['/document/index']],
                                        ['label' => 'Oks', 'icon' => 'user', 'url' => ['/oks/index']],
                                        ['label' => 'Oks Case', 'icon' => 'user', 'url' => ['/oks-case/index']],
                                        ['label' => 'Department', 'icon' => 'user', 'url' => ['/department/index']],
                                        ['label' => 'Geolocation', 'icon' => 'user', 'url' => ['/geolocation/index']],
                                        ['label' => 'Offence', 'icon' => 'user', 'url' => ['/offence/index']],
                                ],
                            ],
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                        ],
                    ],
                    // ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'visible'=>false,
                        'label' => 'Same tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
