<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DepartmentCase */

?>
<div class="department-case-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->file_no) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'department.name',
            'label' => 'Department',
        ],
        'file_no',
        'compound_no',
        [
            'attribute' => 'district.name',
            'label' => 'District',
        ],
        [
            'attribute' => 'subdistrict.name',
            'label' => 'Subdistrict',
        ],
        'address',
        [
            'attribute' => 'offence.name',
            'label' => 'Offence',
        ],
        'commited_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>