<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DepartmentCase */

$this->title = $model->file_no;
$this->params['breadcrumbs'][] = ['label' => 'Department Case', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-case-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Department Case'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'department.name',
                'label' => 'Department'
            ],
        'file_no',
        'compound_no',
        [
                'attribute' => 'district.name',
                'label' => 'District'
            ],
        [
                'attribute' => 'subdistrict.name',
                'label' => 'Subdistrict'
            ],
        'address',
        [
                'attribute' => 'offence.name',
                'label' => 'Offence'
            ],
        'commited_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerCaseRecord->totalCount){
    $gridColumnCaseRecord = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'record_type',
        [
                'attribute' => 'caseStatus.name',
                'label' => 'Case Status'
            ],
        'date',
        'description',
        'requested_date',
        'requested_by',
        'approved',
        'approved_by',
        'approved_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCaseRecord,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Case Record'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnCaseRecord
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerCourtCase->totalCount){
    $gridColumnCourtCase = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'file_no',
                'summon_no',
        'trial_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCourtCase,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Court Case'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnCourtCase
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerDocumentChecklist->totalCount){
    $gridColumnDocumentChecklist = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'name',
        'file',
        'description',
        'completed',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDocumentChecklist,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Document Checklist'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnDocumentChecklist
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerNote->totalCount){
    $gridColumnNote = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'notes',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerNote,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Note'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnNote
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerOksCase->totalCount){
    $gridColumnOksCase = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                [
                'attribute' => 'oks.name',
                'label' => 'Oks'
            ],
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerOksCase,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Oks Case'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnOksCase
    ]);
}
?>
    </div>
</div>
