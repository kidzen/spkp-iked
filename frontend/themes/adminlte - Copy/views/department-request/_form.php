<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\DepartmentCase */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'CaseRecord',
        'relID' => 'case-record',
        'value' => \yii\helpers\Json::encode($model->caseRecords),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'CourtCase',
        'relID' => 'court-case',
        'value' => \yii\helpers\Json::encode($model->courtCases),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'DocumentChecklist',
        'relID' => 'document-checklist',
        'value' => \yii\helpers\Json::encode($model->documentChecklists),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Note',
        'relID' => 'note',
        'value' => \yii\helpers\Json::encode($model->notes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'OksCase',
        'relID' => 'oks-case',
        'value' => \yii\helpers\Json::encode($model->oksCases),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="department-case-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'department_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Department::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Department'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'file_no')->textInput(['maxlength' => true, 'placeholder' => 'File No']) ?>

    <?= $form->field($model, 'compound_no')->textInput(['maxlength' => true, 'placeholder' => 'Compound No']) ?>

    <div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'district_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\District::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose District','id'=>'district-id'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'subdistrict_id')->widget(DepDrop::classname(), [
        'type'=>DepDrop::TYPE_SELECT2,
        'data'=>[null => 'Not Specific'],
        'options' => ['id'=>'subdistrict-id'],
        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
        'pluginOptions'=>[
            'depends'=>['district-id'],
            'placeholder' => 'Choose District',
            'url' => Url::to(['/site/subdistrict'])
        ]
    ]);
    ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-6">
    <?= $form->field($model, 'address')->textArea(['placeholder' => 'Address']); ?>
    </div>

    </div>
    <?= $form->field($model, 'offence_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Offence::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Offence'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'commited_date')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Choose Commited Time'],
        'pluginOptions' => [
            'allowClear' => true,
            'todayHighlight' => true,
            'todayBtn' => true,
            'format' => 'dd-M-yy H:i:s p',
            // 'format' => 'dd-M-Y HH:ii',
            // 'daysOfWeekDisabled' => '0,6',
            // 'hoursDisabled' => '0,1,2,3,4,5,6,7,19,20,21,22,23',
            'autoclose' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CaseRecord'),
            'content' => $this->render('_formCaseRecord', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->caseRecords),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CourtCase'),
            'content' => $this->render('_formCourtCase', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->courtCases),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('DocumentChecklist'),
            'content' => $this->render('_formDocumentChecklist', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->documentChecklists),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Note'),
            'content' => $this->render('_formNote', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->notes),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('OksCase'),
            'content' => $this->render('_formOksCase', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->oksCases),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
