<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DocumentChecklist */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Document Checklist', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-checklist-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Document Checklist'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            <?=
             Html::a('<i class="glyphicon glyphicon-hand-up"></i> ' . 'PDF',
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            <?=
             Html::a('<i class="glyphicon glyphicon-print"></i> ' . 'Download',
                $model->file,
                [
                    'class' => 'btn btn-info',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the attachment PDF file in a new window'
                ]
            )?>

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'case.file_no',
            'label' => 'Case',
        ],
        'name',
        'file',
        'description',
        'completed',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
