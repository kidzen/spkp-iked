<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\DocumentChecklist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-checklist-form">

    <?php //$form = ActiveForm::begin(['options' => ['data-pjax' => true, 'enctype' => 'multipart/form-data']]); ?>
    <?php //$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <?php $form = ActiveForm::begin() ?>
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'case_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\DepartmentCase::find()->orderBy('id')->asArray()->all(), 'id', 'file_no'),
        'options' => ['placeholder' => 'Choose Department case'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?php
    // echo $form->field($model, 'file')->textInput(['maxlength' => true, 'placeholder' => 'File']);
    ?>
    <?php
    echo $form->field($model, 'fileInputBox')->widget(FileInput::classname(), [
        'options'=>['accept'=>'pdf/*'],
        'pluginOptions'=>['allowedFileExtensions'=>['pdf']]
    ]);
    ?>
    <?php
    // echo $form->field($model, 'fileInputBox')->fileInput() ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

    <?= $form->field($model, 'completed')->textInput(['placeholder' => 'Completed']) ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
