<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->departmentCases,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'file_no',
        'compound_no',
        [
                'attribute' => 'caseLot.address',
                'label' => 'Case Lot'
            ],
        [
                'attribute' => 'offence.name',
                'label' => 'Offence'
            ],
        'commited_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'department-case'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
