<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\CaseLot */

?>
<div class="case-lot-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->address) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'district.name',
            'label' => 'District',
        ],
        [
            'attribute' => 'subdistrict.name',
            'label' => 'Subdistrict',
        ],
        'address',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>