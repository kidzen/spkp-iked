<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CaseLot */

$this->title = 'Update Case Lot: ' . ' ' . $model->address;
$this->params['breadcrumbs'][] = ['label' => 'Case Lot', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->address, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="case-lot-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
