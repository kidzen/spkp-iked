<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CourtCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Court Case';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="court-case-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Court Case', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Carian', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        'file_no',
        [
                'attribute' => 'department_file_id',
                'label' => 'Department File',
                'value' => function($model){
                    if ($model->departmentFile)
                    {return $model->departmentFile->file_no;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\DepartmentCase::find()->asArray()->all(), 'id', 'file_no'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Department case', 'id' => 'grid-court-case-search-department_file_id']
            ],
        'summon_no',
        'trial_date',
        'remark',
        [
            'attribute' => 'status', //'visible' => false
            'format' => 'raw',
            'value' => function($model){
                $status = NULL;
                switch ($model->status) {
                    case 11:
                        $status = Html::tag('span','Pending Department Approval',['class'=>'label label-warning']);
                        break;
                    case 12:
                        $stat1 = Html::tag('span','Department Approved',['class'=>'label label-success']);
                        $stat2 = Html::tag('span','Ready For Court',['class'=>'label label-info']);
                        $status = $stat1.Html::tag('br').$stat2;
                        break;
                    case 13:
                        $status = Html::tag('span','Department Rejected',['class'=>'label label-danger']);
                        break;
                    case 14:
                        $stat1 = Html::tag('span','Department Approved',['class'=>'label label-success']);
                        $stat2 = Html::tag('span','Pending Court Approval',['class'=>'label label-warning']);
                        $status = $stat1.Html::tag('br').$stat2;
                        break;
                    case 15:
                        $status = Html::tag('span','Department Disposed',['class'=>'label label-danger']);
                        break;
                    case 21:
                        $status = Html::tag('span','Pending Court Approval',['class'=>'label label-warning']);
                        break;
                    case 22:
                        $status = Html::tag('span','Court Approved',['class'=>'label label-success']);
                        break;
                    case 23:
                        $status = Html::tag('span','Court Rejected',['class'=>'label label-danger']);
                        break;
                    case 24:
                        $status = Html::tag('span','Court File Disposed',['class'=>'label label-danger']);
                        break;
                    case 25:
                        $status = Html::tag('span','Waiting For Trial',['class'=>'label label-info']);
                        break;
                    case 26:
                        $status = Html::tag('span','Case On Trial',['class'=>'label label-info']);
                        break;
                    case 27:
                        $status = Html::tag('span','Court Result',['class'=>'label label-success']);
                        break;
                }
                $requestedBy = isset($model->requestBy)?'Requested<br>by '.$model->requestBy->username:NULL;
                $requestedAt = isset($model->request_at)?'<br>At '.$model->request_at:NULL;
                return $status.Html::tag('span',$requestedBy.$requestedAt,['class'=>'help-block']);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => [1 => 'Approved', 2 => 'Pending', 3 => 'Ready For Trial', 4 => 'Ongoing', 5 => 'Waiting For Trial', 6 => 'Start Trial', 7 => 'End Of Trial', 8 => 'Rejected',],
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-department-case-search-status']
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            // 'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,
            'template' => '{approve} {reject} {view} {update} {delete} {recover}',
            'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
            'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
            // 'deleteOptions' => ['title' => 'Delete', 'data-toggle' => 'tooltip'],
            'buttons' => [
                'approve' => function ($url, $model) {
                    if ($model->status != 22 && $model->status != 23 ) {

                        $button = Html::a('<span class="glyphicon glyphicon-check"></span>', $url, ['title' => Yii::t('yii', 'Approve'), 'data-toggle' => 'tooltip',
                            'data-method' => 'post'
                            ]);
                        $list = Html::tag('li',Html::a('<span class="glyphicon glyphicon-check"></span> Approve', $url, ['title' => Yii::t('yii', 'Approve'), 'data-toggle' => 'tooltip',
                            'data-method' => 'post'
                            ]));
                        return ($dropdown = 0  ? $list : $button);
                    }
                },
                'reject' => function ($url, $model) {
                    if ($model->status != 22 && $model->status != 23 ) {
                        $button = Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip',
                            'data-method' => 'post'
                            ]);
                        $list = Html::tag('li',Html::a('<span class="glyphicon glyphicon-remove"></span> Reject', $url, ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip',
                            'data-method' => 'post'
                            ]));
                        return ($dropdown = 0  ? $list : $button);
                    }
                },
                'recover' => function ($url, $model) {
                    if ($model->deleted === 1) {
                        $button = Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip']);
                        $list = Html::tag('li',Html::a('<span class="glyphicon glyphicon-refresh"></span> Recover', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip',
                            'data-method' => 'post']));
                        return ($dropdown = 0  ? $list : $button);
                    }
                },
                'delete' => function ($url, $model) {
                    if ($model->deleted === 0) {
                        $button = Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                            'data-method' => 'post']);
                        $list = Html::tag('li',Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                            'data-method' => 'post']));
                        return ($dropdown = 0  ? $list : $button);
                    }
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === '') {
                    $url = ['index'];
                    return $url;
                } else if ($action === 'approve') {
                    $url = ['approve','id'=>$model->id];
                    return $url;
                } else if ($action === 'reject') {
                    $url = ['reject','id'=>$model->id];
                    return $url;
                } else if ($action === 'view') {
                    $url = ['view','id'=>$model->id];
                    return $url;
                } else if ($action === 'update') {
                    $url = ['update','id'=>$model->id];
                    return $url;
                } else if ($action === 'delete') {
                    $url = ['delete','id'=>$model->id];
                    return $url;
                } else if ($action === 'recover') {
                    $url = ['recover','id'=>$model->id];
                    return $url;
                }

            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => true,
            // 'dropdownOptions' => [
            //     // 'class' => 'text-center',
            //     // 'width' => '10px'
            // ],
            'template' => '{approve} {reject} {start-task} {task-done}',
            'buttons' => [
                'start-task' => function ($url,$model) {
                    return Html::tag('div',Html::a('Start', ['start-task','id'=>$model->id], ['title' => 'Start this job','class'=>'btn btn-info btn-sm','data-method' => 'POST','data-pjax'=>1,]),['class'=>'text-center']);
                },
                'task-done' => function ($url,$model) {
                    return Html::tag('div',Html::a('Completed', ['request-task-done','id'=>$model->id], ['title' => 'Completed','class'=>'btn btn-success btn-sm','data-method' => 'POST','data-pjax'=>1,]),['class'=>'text-center']);
                },
                'approve' => function ($url,$model) {
                    return Html::tag('div',Html::a('Approve', ['approve','id'=>$model->id], ['title' => 'Approve','class'=>'btn btn-primary btn-sm','data-method' => 'POST','data-pjax'=>1,]),['class'=>'text-center']);
                },
                'reject' => function ($url,$model) {
                    return Html::tag('div',Html::a('Reject', ['reject','id'=>$model->id], ['title' => 'Reject','class'=>'btn btn-danger btn-sm','data-method' => 'POST','data-pjax'=>1,]),['class'=>'text-center']);
                },
            ],
        ],
        [
            'header'=>'Court Action',
            'class' => 'kartik\grid\ActionColumn',
            // 'dropdown' => true,
            'template' => '{start-trial}',
            'buttons' => [
                'start-trial' => function ($url, $model) {
                    // if ($model->status == 22) {
                        // die();
                        return Html::a('<span class="glyphicon glyphicon-play" style="color:green;"></span>', ['/court-request/create','fileId'=>$model->id], [
                        'title' => Yii::t('yii', 'Forward to court'),'data-toggle' => 'tooltip'
                        ]);
                    // }
                }
            ],
            // 'visible'=>Yii::$app->user->isAdmin,
        ],
        [
            'header'=>'Permanent Delete',
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{delete-permanent}',
            'buttons' => [
                'delete-permanent' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                    'title' => Yii::t('yii', 'Hard Delete'),'data-toggle' => 'tooltip'
                    ]);
                }
            ],
            'visible'=>Yii::$app->user->isAdmin,
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-court-case']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
