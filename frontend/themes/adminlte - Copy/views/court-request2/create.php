<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CourtCase */

$this->title = 'Create Court Case';
$this->params['breadcrumbs'][] = ['label' => 'Court Case', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="court-case-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
