<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CourtCase */

$this->title = 'Update Court Case: ' . ' ' . $model->file_no;
$this->params['breadcrumbs'][] = ['label' => 'Court Case', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->file_no, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="court-case-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
