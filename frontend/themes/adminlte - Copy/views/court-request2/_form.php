<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CourtCase */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="court-case-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'file_no')->textInput(['maxlength' => true, 'placeholder' => 'File No']) ?>

    <?= $form->field($model, 'department_file_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\DepartmentCase::find()->orderBy('id')->asArray()->all(), 'id', 'file_no'),
        'options' => ['placeholder' => 'Choose Department case'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'summon_no')->textInput(['maxlength' => true, 'placeholder' => 'Summon No']) ?>

    <?= $form->field($model, 'trial_date')->widget(\kartik\widgets\DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Trial Time'],
        'pluginOptions' => [
            'allowClear' => true,
            'todayHighlight' => true,
            'todayBtn' => true,
            'format' => 'dd-M-yy',
            // 'daysOfWeekDisabled' => '0,6',
            // 'hoursDisabled' => '0,1,2,3,4,5,6,7,19,20,21,22,23',
            'autoclose' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
