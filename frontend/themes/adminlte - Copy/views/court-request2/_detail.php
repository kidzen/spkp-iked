<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\CourtCase */

?>
<div class="court-case-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->file_no) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'file_no',
        [
            'attribute' => 'departmentFile.file_no',
            'label' => 'Department File',
        ],
        'summon_no',
        'trial_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>