<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CaseRecord */

$this->title = 'Create Case Record';
$this->params['breadcrumbs'][] = ['label' => 'Case Record', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="case-record-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
