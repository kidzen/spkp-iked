<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\CaseRecord */

?>
<div class="case-record-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->description) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'case.file_no',
            'label' => 'Case',
        ],
        'record_type',
        [
            'attribute' => 'caseStatus.name',
            'label' => 'Case Status',
        ],
        'date',
        'description',
        'requested_date',
        'requested_by',
        'approved',
        'approved_by',
        'approved_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>