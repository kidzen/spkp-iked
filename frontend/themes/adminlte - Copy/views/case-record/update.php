<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CaseRecord */

$this->title = 'Update Case Record: ' . ' ' . $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Case Record', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->description, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="case-record-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
