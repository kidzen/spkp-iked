<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('DepartmentCase'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Case Record'),
        'content' => $this->render('_dataCaseRecord', [
            'model' => $model,
            'row' => $model->caseRecords,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Court Case'),
        'content' => $this->render('_dataCourtCase', [
            'model' => $model,
            'row' => $model->courtCases,
        ]),
    ],
                        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Document Attachment'),
        'content' => $this->render('_dataDocumentAttachment', [
            'model' => $model,
            'row' => $model->documentAttachments,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Document Checklist'),
        'content' => $this->render('_dataDocumentChecklist', [
            'model' => $model,
            'row' => $model->documentChecklists,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Note'),
        'content' => $this->render('_dataNote', [
            'model' => $model,
            'row' => $model->notes,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Oks Case'),
        'content' => $this->render('_dataOksCase', [
            'model' => $model,
            'row' => $model->oksCases,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
