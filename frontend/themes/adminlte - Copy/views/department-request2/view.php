<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DepartmentCase */

$this->title = $model->file_no;
$this->params['breadcrumbs'][] = ['label' => 'Department Case', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-case-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Department Case'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
<?=
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF',
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'department.name',
            'label' => 'Department',
        ],
        'file_no',
        'compound_no',
        [
            'attribute' => 'caseLot.name',
            'label' => 'Case Lot',
        ],
        [
            'attribute' => 'offence.name',
            'label' => 'Offence',
        ],
        'commited_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerCaseRecord->totalCount){
    $gridColumnCaseRecord = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'record_type',
            [
                'attribute' => 'caseStatus.name',
                'label' => 'Case Status'
            ],
            'description',
            'requested_date',
            'requested_by',
            'approved',
            'approved_by',
            'approved_date',
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCaseRecord,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-case-record']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Case Record'),
        ],
        'columns' => $gridColumnCaseRecord
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerCourtCase->totalCount){
    $gridColumnCourtCase = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'file_no',
                        'summon_no',
            'trial_date',
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCourtCase,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-court-case']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Court Case'),
        ],
        'columns' => $gridColumnCourtCase
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerDocumentAttachment->totalCount){
    $gridColumnDocumentAttachment = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'label',
            'path',
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDocumentAttachment,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-document-attachment']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Document Attachment'),
        ],
        'columns' => $gridColumnDocumentAttachment
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerDocumentChecklist->totalCount){
    $gridColumnDocumentChecklist = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'name',
            'description',
            'completed',
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDocumentChecklist,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-document-checklist']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Document Checklist'),
        ],
        'columns' => $gridColumnDocumentChecklist
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerNote->totalCount){
    $gridColumnNote = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'notes',
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerNote,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-note']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Note'),
        ],
        'columns' => $gridColumnNote
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerOksCase->totalCount){
    $gridColumnOksCase = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'oks.name',
                'label' => 'Oks'
            ],
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerOksCase,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-oks-case']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Oks Case'),
        ],
        'columns' => $gridColumnOksCase
    ]);
}
?>
    </div>
</div>
