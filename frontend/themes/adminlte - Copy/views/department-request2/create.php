<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DepartmentCase */

$this->title = 'Create Department Case';
$this->params['breadcrumbs'][] = ['label' => 'Department Case', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-case-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
