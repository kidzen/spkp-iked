<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\CaseStatus */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Case Status', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="case-status-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Case Status'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'type',
        'description',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerCaseRecord->totalCount){
    $gridColumnCaseRecord = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'case.file_no',
                'label' => 'Case'
            ],
        'record_type',
                'date',
        'description',
        'requested_date',
        'requested_by',
        'approved',
        'approved_by',
        'approved_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCaseRecord,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Case Record'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnCaseRecord
    ]);
}
?>
    </div>
</div>
