<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\CaseStatus */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Case Status', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="case-status-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Case Status'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'type',
        'description',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerCaseRecord->totalCount){
    $gridColumnCaseRecord = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'case.file_no',
                'label' => 'Case'
            ],
            'record_type',
                        'date',
            'description',
            'requested_date',
            'requested_by',
            'approved',
            'approved_by',
            'approved_date',
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCaseRecord,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-case-record']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Case Record'),
        ],
        'columns' => $gridColumnCaseRecord
    ]);
}
?>
    </div>
</div>
