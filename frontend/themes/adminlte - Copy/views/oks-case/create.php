<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OksCase */

$this->title = 'Create Oks Case';
$this->params['breadcrumbs'][] = ['label' => 'Oks Case', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oks-case-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
