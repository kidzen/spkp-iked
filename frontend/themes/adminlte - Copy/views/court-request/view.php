<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\CourtCase */
$boxHeadingTemplate = <<< HTML
<div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
    </button>

    </div>
    <h3 class="box-title">
        {heading}
    </h3>
HTML;
$boxTemplate = <<< HTML
<div class="{prefix}{type} box-solid">
{panelHeading}
    <div class="box-body">
        {panelBefore}{items}
    </div>
</div>
HTML;

$this->title = $model->file_no;
$this->params['breadcrumbs'][] = ['label' => 'Court Case', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="court-case-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Court Case'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
<?=
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF',
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'file_no',
        [
            'attribute' => 'departmentFile.file_no',
            'label' => 'Department File',
        ],
        'summon_no',
        'trial_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

<?php
if($providerDepartmentFile){
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'file_no',
        // [
        //     'attribute' => 'departmentFile.file_no',
        //     'label' => 'Department File',
        // ],
        // 'summon_no',
        // 'trial_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ]; ?>
    <div class="col-md-6">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Fail Jabatan</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
            <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">

    <?php
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
    echo '</div></div></div>';
?>
    </div>
    <div class="row">
        <div class="col-md-6">

<?php
if($providerCaseRecord->totalCount){
    $gridColumnCaseRecord = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'record_type',
            [
                'attribute' => 'caseStatus.name',
                'label' => 'Case Status'
            ],
            'date',
            'description',
            // 'requested_date',
            // 'requested_by',
            'approved',
            // 'approved_by',
            // 'approved_date',
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo '<div class="col-md-12">';
    echo Gridview::widget([
        'dataProvider' => $providerCaseRecord,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-case-record']],
        'panelPrefix' => 'box box-',
        'panelTemplate' => $boxTemplate,
        'panelHeadingTemplate' => $boxHeadingTemplate,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'headingOptions' => ['class'=>'box-header with-border'],
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Case Record'),
        ],
        'columns' => $gridColumnCaseRecord
    ]);
    echo '</div>';
}
}

if($providerDocumentChecklist->totalCount){
    $gridColumnDocumentChecklist = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'name',
            'file',
            'description',
            'completed',
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo '<div class="col-md-6">';
    echo Gridview::widget([
        'dataProvider' => $providerDocumentChecklist,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-document-checklist']],
        'panelPrefix' => 'box box-',
        'panelPrefix' => 'box box-',
        'panelTemplate' => $boxTemplate,
        'panelHeadingTemplate' => $boxHeadingTemplate,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'headingOptions' => ['class'=>'box-header with-border'],
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Document Checklist'),
        ],
        'columns' => $gridColumnDocumentChecklist
    ]);
    echo '</div></div>';
}
?>

<?php
if($providerNote->totalCount){
    $gridColumnNote = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'notes',
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo '<div class="col-md-6">';
    echo Gridview::widget([
        'dataProvider' => $providerNote,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-note']],
        'panelPrefix' => 'box box-',
        'panelTemplate' => $boxTemplate,
        'panelHeadingTemplate' => $boxHeadingTemplate,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'headingOptions' => ['class'=>'box-header with-border'],
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Note'),
        ],
        'columns' => $gridColumnNote
    ]);
    echo '</div>';
}
?>

<?php
if($providerOksCase->totalCount){
    $gridColumnOksCase = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'oks.name',
                'label' => 'Oks'
            ],
            'remark',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo '<div class="col-md-6">';
    echo Gridview::widget([
        'dataProvider' => $providerOksCase,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-oks-case']],
        'panelPrefix' => 'box box-',
        'panelTemplate' => $boxTemplate,
        'panelHeadingTemplate' => $boxHeadingTemplate,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'headingOptions' => ['class'=>'box-header with-border'],
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Oks Case'),
        ],
        'columns' => $gridColumnOksCase
    ]);
    echo '</div>';
}
?>
    </div>
</div>



