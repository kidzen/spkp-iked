<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('CourtCase'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Case Record'),
        'content' => $this->render('_dataCaseRecord', [
            'model' => $model,
            'row' => $model->departmentFile->caseRecords,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Document Checklist'),
        'content' => $this->render('_dataDocumentChecklist', [
            'model' => $model->departmentFile,
            'row' => $model->departmentFile->documentChecklists,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Note'),
        'content' => $this->render('_dataNote', [
            'model' => $model->departmentFile,
            'row' => $model->departmentFile->notes,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Oks Case'),
        'content' => $this->render('_dataOksCase', [
            'model' => $model->departmentFile,
            'row' => $model->departmentFile->oksCases,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
