<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\CourtCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-court-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'file_no')->textInput(['maxlength' => true, 'placeholder' => 'File No']) ?>

    <?= $form->field($model, 'department_file_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\DepartmentCase::find()->orderBy('id')->asArray()->all(), 'id', 'file_no'),
        'options' => ['placeholder' => 'Choose Department case'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'summon_no')->textInput(['maxlength' => true, 'placeholder' => 'Summon No']) ?>

    <?= $form->field($model, 'trial_date')->textInput(['placeholder' => 'Trial Date']) ?>

    <?php /* echo $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) */ ?>

    <?php /* echo $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
