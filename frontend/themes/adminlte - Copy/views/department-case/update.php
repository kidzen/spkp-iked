<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DepartmentCase */

$this->title = 'Update Department Case: ' . ' ' . $model->file_no;
$this->params['breadcrumbs'][] = ['label' => 'Department Case', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->file_no, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="department-case-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
