<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Offence */

$this->title = 'Create Offence';
$this->params['breadcrumbs'][] = ['label' => 'Offence', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offence-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
