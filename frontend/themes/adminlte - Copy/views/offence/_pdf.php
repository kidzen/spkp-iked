<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Offence */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Offence', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offence-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Offence'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'act',
        'section',
        'name',
        'description',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerDepartmentCase->totalCount){
    $gridColumnDepartmentCase = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'department.name',
                'label' => 'Department'
            ],
        'file_no',
        'compound_no',
        [
                'attribute' => 'caseLot.address',
                'label' => 'Case Lot'
            ],
                'commited_date',
        'remark',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDepartmentCase,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Department Case'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnDepartmentCase
    ]);
}
?>
    </div>
</div>
