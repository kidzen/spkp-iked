<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftStatus */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Status Draf', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-status-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Status'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti mahu PADAM data ini?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerDraftDocument->totalCount){
    $gridColumnDraftDocument = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
            'file',
            [
                'attribute' => 'contract.name',
                'label' => 'Contract'
            ],
                        'description',
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftDocument,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-document']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Draf'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftDocument
    ]);
}
?>
    </div>
</div>
