<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DraftStatus */

$this->title = 'Tambah Draft Status';
$this->params['breadcrumbs'][] = ['label' => 'Status Draf', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
