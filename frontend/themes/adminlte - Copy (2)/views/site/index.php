<?php

/* @var $this yii\web\View */

$this->registerCss('
.intro-text{
    text-transform: capitalize;
    font-family: arial;
    word-spacing: 10px;
    text-align: center;
  font-size: 40px;
  font-weight: bold;
  font-family: sans-serif;
  color: white;

}
');
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1 class="intro-text" style="text-shadow: 3px 3px #0f0e0e;color: lightsteelblue;">Sistem Perjanjian Majlis Perbandaran Seberang Perai</h1>
    </div>
</div>

