<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ParamList */

$this->title = 'Tambah Param List';
$this->params['breadcrumbs'][] = ['label' => 'Senarai Pemboleh Ubah', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
