<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DocumentChecklistLookup */

$this->title = 'Tambah Rujukan Senarai Semak';
$this->params['breadcrumbs'][] = ['label' => 'Rujukan Senarai Semak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-checklist-lookup-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
