<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draft Document', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-document-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Document'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti mahu PADAM data ini?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        [
            'attribute' => 'contract.name',
            'label' => 'Kontrak',
        ],
        [
            'attribute' => 'draftStatus.name',
            'label' => 'Status Draf',
        ],
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerDraftParam->totalCount){
    $gridColumnDraftParam = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'paramList.name',
                'label' => 'Senarai Pemboleh Ubah'
            ],
            'param',
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftParam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-param']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pemboleh Ubah Draf'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftParam
    ]);
}
?>
    </div>
</div>
