<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DraftDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Draft Document';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="draft-document-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Draft Document', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Carian', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        [
                'attribute' => 'contract_id',
                'label' => 'Kontrak',
                'value' => function($model){
                    if ($model->contract)
                    {return $model->contract->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Contract::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Contract', 'id' => 'grid-draft-document-search-contract_id']
            ],
        [
                'attribute' => 'draft_status_id',
                'label' => 'Status Draf',
                'value' => function($model){
                    if ($model->draftStatus)
                    {return $model->draftStatus->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\DraftStatus::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Draft status', 'id' => 'grid-draft-document-search-draft_status_id']
            ],
        'description',
        'remark',
        [
            'class' => 'yii\grid\ActionColumn','visible'=>!Yii::$app->user->isUser,
        ],
        [
            'class' => 'kartik\grid\ActionColumn','visible'=>!Yii::$app->user->isUser,
            'template' => '{download} {checklist} {generate}',
            'buttons' => [
                'generate' => function($url, $model) {
                    // if(empty($model->file)){
                        return Html::a('<span class="fa fa-file-word-o" style="color:green;"></span> draft',['/template-generator/generate-draft','id'=>$model->id]);
                    // }
                },
                'download' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span> muat turun',$model->file,['data-pjax'=>0]);
                },
            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-document']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
