<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\DocumentChecklist */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="document-checklist-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'doc_check_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\DocumentChecklistLookup::find()->orderBy('id')->asArray()->all(), 'id', 'description','category'),
        'options' => ['placeholder' => 'Choose Rujukan Senarai Semak'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'contract_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Contract::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Pilih Kontrak'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'file')->textInput(['maxlength' => true, 'placeholder' => 'Fail']) ?>

    <?= $form->field($model, 'completed')->widget(CheckboxX::classname(), ['pluginOptions'=>['threeState'=>false]]) ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Catatan']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Kemaskini', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
