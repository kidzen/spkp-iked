<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DocumentChecklist */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Document Checklist', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-checklist-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Document Checklist'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti mahu PADAM data ini?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'docCheck.name',
            'label' => 'Doc Check',
        ],
        [
            'attribute' => 'contract.name',
            'label' => 'Kontrak',
        ],
        'completed',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
