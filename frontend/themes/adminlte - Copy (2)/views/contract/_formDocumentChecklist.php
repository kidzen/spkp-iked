<div class="form-group" id="add-document-checklist">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use kartik\checkbox\CheckboxX;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'DocumentChecklist',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        // "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'doc_check_id' => [
            'label' => 'Rujukan Senarai Semak',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\DocumentChecklistLookup::find()->orderBy('name')->asArray()->all(), 'id', 'description','category'),
                'options' => ['placeholder' => 'Choose Rujukan Senarai Semak'],
            ],
            // 'columnOptions' => ['width' => '200px']
        ],
        // 'completed' => [
        //     'type'=>TabularForm::INPUT_WIDGET,
        //     'widgetClass'=>\kartik\widgets\SwitchInput::classname(),
        //     'options'=>[
        //         // 'autoLabel' => true,
        //         'pluginOptions' => [
        //             'enclosedLabel' => true,
        //             'threeState' => false,

        //             // 'handleWidth'=>600,
        //             'onText'=>'YA',
        //             'offText'=>'TIDAK'
        //         ],
        //     ],
        //     // 'columnOptions' => ['width' => '200px']
        // ],
        "completed" => ['type' => TabularForm::INPUT_TEXT],
        'remark' => [
            'type' => TabularForm::INPUT_TEXT,
            // 'columnOptions' => ['width' => '200px']
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowDocumentChecklist(' . $key . '); return false;', 'id' => 'document-checklist-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Tambah Senarai Semak Draf', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowDocumentChecklist()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

