<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->documentChecklists,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'docCheck.description',
                'label' => 'Senarai Semak / Lampiran'
            ],
        [
                'attribute' => 'completed',
                'label' => 'Selesai / Belum Selesai'
            ],
        [
                'attribute' => 'remark',
                'label' => 'Catatan'
            ],
        [
            'class' => 'yii\grid\ActionColumn','visible'=>!Yii::$app->user->isUser,
            'controller' => 'document-checklist'
        ],
    ];
    ?>

        <?= \yii\helpers\Html::a('Muat Naik Template', ['/document-checklist/create'], ['class' => 'btn btn-primary']) ?>
<?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
