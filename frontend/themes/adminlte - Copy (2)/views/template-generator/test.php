<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Template Generator';
?>

<div class="draft-document-form">
<h1>Jana Draf</h1>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4>Perincian Kontrak</h4>
        </div>
        <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
            <div class="row">

                <?= $form->errorSummary($model); ?>
                <div class="col-md-3">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Nama','autofocus' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'contract_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\Contract::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                        'options' => ['placeholder' => 'Pilih Kontrak'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Keterangan']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Catatan']) ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-3">
                    <?= $form->field($model, 'file')->textInput(['maxlength' => true, 'placeholder' => 'Fail']) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4>Perincian Draf</h4>
                </div>
                <div class="panel-body">
                <?php
                    foreach ($paramList as $key => $value) {
                        echo '<div class="col-md-4">';
                        echo $form->field($dynaModel, $value)->textInput(['maxlength' => true]);
                        echo '</div>';
                    }
                ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Kemaskini', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
