<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftParam */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Draft Param', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-param-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Param'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti mahu PADAM data ini?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'draftDocument.name',
            'label' => 'Draft Document',
        ],
        [
            'attribute' => 'paramList.name',
            'label' => 'Senarai Pemboleh Ubah',
        ],
        'param',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
