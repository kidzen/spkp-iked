<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DateSetting */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="date-setting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

    <?= $form->field($model, 'step_refference')->textInput(['maxlength' => true, 'placeholder' => 'Step Refference']) ?>

    <?= $form->field($model, 'duration')->textInput(['maxlength' => true, 'placeholder' => 'Duration']) ?>

    <?= $form->field($model, 'duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Duration Type']) ?>

    <?= $form->field($model, 'from')->textInput(['maxlength' => true, 'placeholder' => 'From']) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Keterangan']) ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Catatan']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Kemaskini', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
