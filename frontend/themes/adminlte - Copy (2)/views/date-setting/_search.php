<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\DateSettingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-date-setting-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

    <?= $form->field($model, 'step_refference')->textInput(['maxlength' => true, 'placeholder' => 'Step Refference']) ?>

    <?= $form->field($model, 'duration')->textInput(['maxlength' => true, 'placeholder' => 'Duration']) ?>

    <?= $form->field($model, 'duration_type')->textInput(['maxlength' => true, 'placeholder' => 'Duration Type']) ?>

    <?php /* echo $form->field($model, 'from')->textInput(['maxlength' => true, 'placeholder' => 'From']) */ ?>

    <?php /* echo $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Keterangan']) */ ?>

    <?php /* echo $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Catatan']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Carian', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Isi Semula', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
