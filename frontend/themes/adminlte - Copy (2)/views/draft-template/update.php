<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DraftTemplate */

$this->title = 'Kemaskini Draft Template: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draft Template', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="draft-template-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
