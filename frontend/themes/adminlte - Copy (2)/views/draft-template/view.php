<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftTemplate */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draft Template', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-template-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Template'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti mahu PADAM data ini?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerContract->totalCount){
    $gridColumnContract = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'contract_no',
            'name',
                        'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContract,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contract']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kontrak'),
        ],
        'export' => false,
        'columns' => $gridColumnContract
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerParamList->totalCount){
    $gridColumnParamList = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'name',
            'param',
            'hint',
            'description',
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerParamList,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-param-list']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Senarai Pemboleh Ubah'),
        ],
        'export' => false,
        'columns' => $gridColumnParamList
    ]);
}
?>
    </div>
</div>
