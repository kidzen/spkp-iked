<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class TemplateGeneratorForm extends Model
{
    public $templateId;
    public $contractId;
    // public $year = date('Y');
    // public $month = date('m');
    public $year = 2017;
    public $month = 7;
    public $title;
    // template parameter
    public $companyName;
    public $companyRegNo;
    public $contractTitle;
    public $contractNo;
    public $currentYear;
    public $contractDurationText;
    public $jobDescription;
    public $agreementStartDate;
    public $agreementEndDate;
    public $contractDuration;
    public $contractStartDate;
    public $contractEndDate;
    public $cost;
    public $irtDuration;
    public $rtDuration;
    public $deliveryDuration;
    public $warrantyDuration;
    public $bon;
    public $compensation;
    public $companyAddress;
    public $companyContactNo;
    public $companyFaxNo;
    public $contractorName1;
    public $contractorIc1;
    public $contractorPosition1;
    public $contractorName2;
    public $contractorIc2;
    public $contractorPosition2;
    public $product;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['templateId','contractId','year','month'],'integer'],
            [['title','companyName','companyRegNo','contractTitle','contractNo','currentYear','contractDurationText','jobDescription','contractDuration','agreementStartDate','agreementEndDate','contractStartDate','contractEndDate','cost','rtDuration','deliveryDuration','warrantyDuration','bon','compensation','companyAddress','companyContactNo','companyFaxNo','contractorName1','contractorIc1','contractorPosition1','contractorName2','contractorIc2','contractorPosition2','product'],'string', 'max' => 255],
            // [['created_at', 'updated_at'], 'safe'],
            // [['approve_level', 'remark'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            // 'verifyCode' => 'Verification Code',
            'templateId' => 'Template',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    // public function sendEmail($email)
    // {
    //     return Yii::$app->mailer->compose()
    //         ->setTo($email)
    //         ->setFrom([$this->email => $this->name])
    //         ->setSubject($this->subject)
    //         ->setTextBody($this->body)
    //         ->send();
    // }

    public function choose($templateId)
    {
        if (!$this->validate()) {
            return null;
        }

        $model = new Generator($templateId);

        // $model->username = $this->username;
        // $model->email = $this->email;
        // $model->setPassword($this->password);
        // $model->generateAuthKey();

        return $user->save() ? $user : null;
    }
}
