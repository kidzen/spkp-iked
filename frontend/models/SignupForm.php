<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $staff_no;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['staff_no', 'trim'],
            ['staff_no', 'required'],
            ['staff_no', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Anda telah berdaftar. Sila dapatkan khidmat admin sekiranya anda terlupa kata laluan.'],
            // ['staff_no', 'exist', 'targetClass' => '\common\models\Profile', 'message' => 'Rekod anda tiada dalam simpanan MPSP.'],
            ['staff_no', 'string', 'min' => 3, 'max' => 255],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->staff_no = $this->staff_no;
        $user->role_id = 3;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }
}
