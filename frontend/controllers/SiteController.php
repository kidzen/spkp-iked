<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','index'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionDashboard()
    {
        $userAchievements = \common\models\UserAchievement::find()
        // ->select('user.staff_no,achievement.name')
        // ->joinWith('achievement')
        // ->joinWith('user')
        // ->joinWith('user.rank')
        // ->joinWith('user.rank.nextRank')
        // ->asArray()
        ->all();

        $rankRequirement = \common\models\RankRequirement::find()
        // ->where(['rank_id' => 1])
        // ->joinWith('achievement')
        // ->joinWith('rank')
        // ->asArray()
        ->all();
        foreach ($rankRequirement as $key => $value) {
            $data2[$value['rank']['id']]['rank_id'] = $value['rank']['id'];
            $data2[$value['rank']['id']]['achievements_id'][$value['achievement']['id']] = $value['achievement']['id'];
        }
        // var_dump($data2);
        foreach ($userAchievements as $key => $value) {
            $data[$value['user']['id']]['staff_no'] = $value['user']['staff_no'];
            $data[$value['user']['id']]['rank_id'] = $value['user']['rank_id'];
            $data[$value['user']['id']]['next_rank_id'] = $value['user']['rank']['next_rank'];
            $data[$value['user']['id']]['achievements_id'][$value['achievement']['id']] = $value['achievement']['id'];
            if(isset($data2[$value['user']['rank']['next_rank']])){
                $data[$value['user']['id']]['achievement_required'] = $data2[$value['user']['rank']['next_rank']]['achievements_id'];
                $data[$value['user']['id']]['achievement_completed'] = array_search($value['achievement']['id'], $data2[$value['user']['rank']['next_rank']]['achievements_id']);
            }
            if(isset($data[$value['user']['id']]['achievement_completed'])) {
                $data[$value['user']['id']]['achievement_completed'] = false || $data[$value['user']['id']]['achievement_completed'];
            } else {
                $data[$value['user']['id']]['achievement_completed'] = false;
            }
        }
        $data3[] = '';
        // var_dump($data);die;
        foreach ($data as $key => $value) {
            // var_dump($key);
            // var_dump($value['achievement_completed']);
            if($value['achievement_completed']){
                $data3[] = $key;
            }
        }
        // var_dump($data);
        // var_dump($data2);
        // var_dump($data3);
        // die;
        $searchModel = new \common\models\search\UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query(andWhere);
        $dataProvider->query->andFilterWhere(['in','id',$data3]);

        // var_dump($dataProvider->query);
        // die;
        // return $this->render('\..\user\index', [
        return $this->render('dashboard', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIndex($id = null)
    {

        // // if(Yii::$app->request->post()){
        // //     $test = $this->redirect('http://localhost/credential/CredentialApi.php');
        // //     var_dump($test);
        // //     die();
        //     // var_dump(Yii::$app->request->post());
        //     // var_dump($id);
        //        // die();
        //     return $this->render('index',['id'=>$id]);
        //     // var_dump('expression');die;
        // }
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        // var_dump(\Yii::$app->user->id);die;
        $this->layout = '\..\..\themes\adminlte\views\layouts\main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = '\..\..\themes\adminlte\views\layouts\main-login.php';
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
